import { controls } from '../../constants/controls';

const {PlayerOneAttack, PlayerOneBlock, PlayerTwoAttack, PlayerTwoBlock, PlayerOneCriticalHitCombination, PlayerTwoCriticalHitCombination} = controls;

export const state = {
  firstFighter: {},
  secondFighter: {},
  firstFighterCurrentHealth: null,
  secondFighterCurrentHealth: null,
  initialWidthHealthIndicator: null,
  isFirstFighterBlockActivated: false,
  isSecondFighterBlockActivated: false,
  timeFirstFighterCriticalHit: null,
  timeSecondFighterCriticalHit: null,
  firstFighterKeysArray: [],
  secondFighterKeysArray: []
} 

export async function fight(firstFighter, secondFighter) {
  setInitialState(firstFighter, secondFighter);
  return new Promise((resolve) => {
    document.addEventListener('keydown', keyDownHandler.bind(null, firstFighter, secondFighter, resolve));
    document.addEventListener('keyup', keyUpHandler);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender); 

  if(damage > 0){
    return damage;
  }
  return 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const attack = fighter.attack;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  if (!fighter) {
    return 0;
  }
  const dodgeChance = Math.random() + 1;
  const defense = fighter.defense;
  return defense * dodgeChance;
}

export function getCriticalHitPower(fighter) {
  return fighter.attack * 2;
}

export function decreaseHealth(isFirstFighterAttack, damage) { 
  const currentHealth = isFirstFighterAttack ? 'secondFighterCurrentHealth' : 'firstFighterCurrentHealth'
  state[currentHealth] = state[currentHealth] - damage;
}

export function decreaseHealthIndicator(isFirstFighterAttack) {
  const defender = isFirstFighterAttack ? state.secondFighter : state.firstFighter;
  const currentHealth = isFirstFighterAttack ? state.secondFighterCurrentHealth : state.firstFighterCurrentHealth;
  const position = isFirstFighterAttack ? 'right' : 'left';

  const healthIndicator = document.getElementById(`${position}-fighter-indicator`);

  const updatedWidth = state.initialWidthHealthIndicator * currentHealth / defender.health;
 
  healthIndicator.style.width = updatedWidth > 0 ? updatedWidth + 'px' : 0;  
}

function setInitialState(firstFighter, secondFighter) {
  state.firstFighter = firstFighter;
  state.secondFighter = secondFighter;
  state.firstFighterCurrentHealth = firstFighter.health;
  state.secondFighterCurrentHealth = secondFighter.health;

  const healthIndicator = document.getElementById(`left-fighter-indicator`);
  state.initialWidthHealthIndicator = healthIndicator.clientWidth;  
}

export function keyDownHandler(firstFighter, secondFighter, resolve, event) {
  const key = event.code;
  let args;
  
  switch (key) {
    case PlayerOneAttack:  
      if (state.isFirstFighterBlockActivated) return;
      args = state.isSecondFighterBlockActivated ? [firstFighter, secondFighter] : [firstFighter];
      decreaseHealth(true, getDamage(...args));
      decreaseHealthIndicator(true);
      break;    
    case PlayerTwoAttack:  
      if (state.isSecondFighterBlockActivated) return;
      args = state.isFirstFighterBlockActivated ? [secondFighter, firstFighter] : [secondFighter];
      decreaseHealth(false, getDamage(...args));
      decreaseHealthIndicator(false);
      break;
    case PlayerOneBlock:
      state.isFirstFighterBlockActivated = true; 
      break;
    case PlayerTwoBlock:      
      state.isSecondFighterBlockActivated = true;
      break;
  }

  if (PlayerOneCriticalHitCombination.includes(key) && !state.firstFighterKeysArray.includes(key)) {
    state.firstFighterKeysArray.push(key)
  } else if (PlayerTwoCriticalHitCombination.includes(key) && !state.secondFighterKeysArray.includes(key)) {
    state.secondFighterKeysArray.push(key)
  }

  if (state.firstFighterKeysArray.length === 3 && state.timeFirstFighterCriticalHit + 10000 < Date.now()) {
    decreaseHealth(true, getCriticalHitPower(firstFighter));
    decreaseHealthIndicator(true);
    state.timeFirstFighterCriticalHit = Date.now();
  } else if (state.secondFighterKeysArray.length === 3 && state.timeSecondFighterCriticalHit + 10000 < Date.now()) {
    decreaseHealth(false, getCriticalHitPower(secondFighter));
    decreaseHealthIndicator(false);
    state.timeSecondFighterCriticalHit = Date.now();
  }

  if (state.firstFighterCurrentHealth < 0) {
    resolve(secondFighter)
  } else if (state.secondFighterCurrentHealth < 0) {
    resolve(firstFighter)
  }
}

export function keyUpHandler(event) {
  const key = event.code;

  switch (key) {
    case PlayerOneBlock:
      state.isFirstFighterBlockActivated = false;  
      break; 
    case PlayerTwoBlock:      
      state.isSecondFighterBlockActivated = false;
      break;
  }

  if (state.firstFighterKeysArray.includes(key)) {
    const pos = state.firstFighterKeysArray.indexOf(key);
    state.firstFighterKeysArray.splice(pos, 1);
  } else if (state.secondFighterKeysArray.includes(key)) {
    const pos = state.secondFighterKeysArray.indexOf(key);
    state.secondFighterKeysArray.splice(pos, 1);
  }
}

